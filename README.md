
# Awall - Atlantikwall Museum Exhibition Android App

Android App built for evaluation of Atlantikwall exhibition at Museon in Den Haag. This application has been built on the Ionic Cordova stack. The user first chooses language(Dutch or English), then perspective(Soldier, Civilian or Civil Servant) and may then watch videos for the various points of interest. Videos are expected to be stored on the phones SD card.

## NB This code base does not inclide the video files.

Video of app in use available at http://youtu.be/LqtbeKY5oV4