// NB all times should be UTC - not local time
var db = null;



var awall = angular.module('awall', [ 'ionic' , 'ngCordova']);


awall.config(function($ionicConfigProvider) {
	  $ionicConfigProvider.views.transition('none');
});
                
var points = [ {
	title : "Clingendael",
	folder: "C3-Seyss-Inquart",
	id : 'C1',
	resource : 'img/01.png'
},  {
	title : "Malieveld",
	folder: "C4-Malieveld",
	id : 'C2',
	resource : 'img/02.png'
}, {
	title : "Oostduinpark",
	folder: "C1-Dunes",
	id : 'C3',
	resource : 'img/03.png'
}, {
	title : "Strandweg",
	folder: "C5-Boulevard",
	id : 'C4',
	resource : 'img/04.png'
},{
	title : "Scheveningen haven",
	folder: "C6-Harbour",
	id : 'C5',
	resource : 'img/05.png'
}, {
	title : "Duindorp",
	folder: "C7-Duindorp",
	id : 'C6',
	resource : 'img/06.png'
},  {
	title : "President Kennedylaan",
	folder: "C8-Duinoord",
	id : 'C7',
	resource : 'img/07.png'
},{
	title : "World Forum", // a.k.a. International-Zone
	folder: "C9-International-Zone",
	id : 'C8',
	resource : 'img/08.png'
}, {
	title : "Scheveningse Bosjes",
	folder: "C11-Scheveningse-Bosjes",
	id : 'C9',
	resource : 'img/09.png'
}, {
	title : "Vredespaleis",
	folder: "C12-Vredespaleis",
	id : 'C10',
	resource : 'img/10.png'
} ];

function getPoint(point_id)
{
	for (i = 0; i < points.length; ++i) {
	    if(point_id == points[i]['id'])
	    	{
	    	return points[i]
	    	}
	}
	return null;
}


function getPerspective(perspective_id){
	
	switch(String(perspective_id)) {
	    case "1": // Duitser
	        return "Duitser";
	        break;
	    case "2":
	    	return "Ambtenaar";
	    	break;
	    case "3":
	    	return "Burger";
	        break;
		case "4": // German
			return "German";
			break;
		case "5":
			return "Official";
			break;
		case "6":
			return "Civilian";
		    break;
		default:
		    //do nothing
	}
	return null;
}




// database activity can only be done when the onDeviceReady() method has fired
awall.run(function($ionicPlatform, $cordovaSQLite) {
	
	$ionicPlatform.registerBackButtonAction(function (event) {
	    event.preventDefault();
	}, 100);
	
    $ionicPlatform.ready(function() {
        if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }
        if (window.cordova) {
        	// According to documents should be able to instantiate with an object
            // db = $cordovaSQLite.openDB({ name: "awall.db" }); //device
        	// in reality have to instantiate with a string
        	// db = $cordovaSQLite.openDB("awall.db"); //device
        	// try bypassing cordova altogether - even recommended on the cordovaSQLite plugin page
        	db = window.sqlitePlugin.openDatabase({name: "awall.db", location: 1});
        }else{
            db = window.openDatabase("awall.db", '1', 'awall', 1024 * 1024 * 100); // browser
        }

        $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS users (id integer primary key, start_datetime datetime default current_timestamp, end_datetime datetime defaut null)");
        $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS visits (id integer primary key, user_id integer not null, language_id integer default 0, perspective_id integer default 0, point_id integer default 0, datetime datetime default current_timestamp)");
    });
});

awall.config(function($stateProvider, $urlRouterProvider) {
	$stateProvider.state('intro', {
		cache: false,
		url : '/',
		templateUrl : 'templates/languages.html',
		controller : 'LanguageController'
	})
	.state('perspectives', {
		cache: false,
		url : '/perspectives/:languageid',
		templateUrl : 'templates/perspectives.html',
		controller : 'PerspectiveController'
	})
	.state('points', {
		cache: false,
		url : '/points/:languageid/:itemid',
		templateUrl : 'templates/points.html',
		controller : 'PointsController'
	}).state('detail', {
		cache: false,
		url : '/detail/:languageid/:itemid/:pointid',
		templateUrl : 'templates/point_detail.html',
		controller : 'PointController'
	}).state('checkout', {
		cache: false,
		url : '/checkout/:languageid',
		templateUrl : 'templates/checkout.html',
		controller : 'CheckoutController'
	});

	$urlRouterProvider.otherwise("/");
});


awall.controller('LanguageController', function($scope, $cordovaSQLite) {
	// will execute when device is ready, or immediately if the device is already ready.
	ionic.Platform.ready(function(){
		var query = "INSERT INTO users (id) VALUES (null)";
	    $cordovaSQLite.execute(db, query, []).then(function(res) {null
	    	// Store the userid to localstorage
	    	window.localStorage['userid'] = res.insertId;
	    }, function (err) {
	        console.error(err);
	    });
	    query=null;
	 });
        
	$scope.languages = [
	    			{
	    				title : "Nederlands",
	    				id : 1,
	    			},
	    			{
	    				title : "English",
	    				id : 2,
	    			}];
});


awall.controller('PerspectiveController', function($stateParams, $scope) {
	$scope.languageid=$stateParams.languageid;

	switch($stateParams.languageid) {
    case "1": // Dutch
        $scope.title='Kies je verhaal';
    	$scope.tasks = [
    	    			{
    	    				title : "Duitser",
    	    				id : 1,
    	    				resource : 'img/duitser1.png',
    	    				description : "Deze bierpul kregen Duitse soldaten tijdens Kerstmis 1943 als herinnering aan de legering langs de Atlantikwall."
    	    			},
    	    			{
    	    				title : "Ambtenaar",
    	    				id : 2,
    	    				resource : 'img/ambtenaar2.png',
    	    				description : "De Duitse bezetter had een avondklok ingesteld. Als je na acht uur’s avonds beroepshalve op straat moest zijn, had je een 'Sonderausweis' nodig."
    	    				
    	    			},
    	    			{
    	    				title : "Burger",
    	    				id : 3,
    	    				resource : 'img/burger3.png',
    	    				description : "Thee was niet meer verkrijgbaar, maar met ingrediënten als eikels, witlof, appel en krokus viel iets te maken wat op thee leek."
    	    			}];
        break;
	    case "2": // English
	    	$scope.title='Choose your story';
	    	$scope.tasks = [
						{
							title : "German",
							id : 4,
							resource : 'img/german4.png',
							description : "This booklet helped German soldiers communicate with the Dutch population."
						},
						{
							title : "Official",
							id : 5,
							resource : 'img/official5.png',
							description : "The German occupation authorities had imposed a curfew. If you had professional reasons for being out on the streets after eight o’clock in the evening, you had to wear this bracelet.",
						},
						{
							title : "Civilian",
							id : 6,
							resource : 'img/civilian6.png',
							description : "Genuine sugar was in short supply; this was a substitute sweetener."
						} ];
	    break;
	    default:
	        //do nothing
		} 
});

awall.controller('PointsController', function($stateParams, $scope) {
	$scope.languageid=$stateParams.languageid;
	$scope.points = points;
	$scope.itemid = $stateParams.itemid;
	$scope.perspective = getPerspective($stateParams.itemid);

	switch($stateParams.languageid) {
    case "1": // Dutch
    	$scope.title='Kies je bestemming';
    	$scope.checkout='Uitchecken';
    	 break;
    case "2": // English
    	$scope.title='Choose your destination';
    	$scope.checkout='Checkout';
    	 break;
    default:
        //do nothing
	}
});

awall.controller('PointController', function($stateParams, $scope, $cordovaSQLite, $state) {

	var pointid = $stateParams.pointid;
	var itemid = $stateParams.itemid;
	var languageid = $stateParams.languageid;
	var perspectiveid=$stateParams.itemid;
	var perspective = getPerspective(perspectiveid);
	var point = getPoint(pointid);
	// for galaxy tab
	//var folder = 'file:///storage/extSdCard/Android/data/awall/files/' + point.folder + '/';
	// for alcatel onetouch pixi
	var folder = 'file:///mnt/sdcard/Android/data/awall/files/' + point.folder + '/';
	

	// Get the userid from local storage
	var userid=window.localStorage['userid'];

	// will execute when device is ready, or immediately if the device is already ready.
	ionic.Platform.ready(function(){
		// Log viewing the point to the database
	    var query = "INSERT INTO visits (user_id, language_id, perspective_id, point_id) VALUES (?,?,?,?)";
	    $cordovaSQLite.execute(db, query, [userid, languageid, perspectiveid, pointid]).then(function(res) {
	    }, function (err) {
	        console.error(err);
	    });
	    query = null;
	 });
	 
	// Set up the action for the back button
	 $scope.back = function (language_id, perspective_id ) {
		 // pause the video
		 var vid = document.getElementById("poi_video");
		 vid.pause();
		 vid=null;
		 
		 // redirect back to list of points for this language and perspective
		 $state.go('points', {languageid: language_id, itemid: perspective_id}, {reload: true});
	}
	 
	// format is point name/language/perspective
	switch(languageid) {
    case "1": // Dutch
    	folder += "Dutch/";
    	$scope.back_text = "< Terug";
    	 break;
    case "2": // English
    	folder += "English/";
    	$scope.back_text = "< Back";
    	 break;
    default:
        //do nothing
	}
	
	// Get file name based on perspective id
	switch(String(perspectiveid)) {
	    case "1": // Duitser
	    case "4": // German	
	    	folder +="soldiers.mp4"
	        break;
	    case "2": //Ambtenaar
	    case "5": //Official
	    	folder +="civilservants.mp4";
	    	break;
	    case "3": //Burger
	    case "6": //Civilian
	    	folder +="civilians.mp4";
	        break;
		default:
		    //do nothing
	}
	
	$scope.point = {
			perspective : perspective,
			title : point.title,
			media : folder
		};
	$scope.perspective_id = perspectiveid;
	$scope.language_id = languageid;
	

	// some clean-up
	//userid=
	pointid = itemid = languageid = perspectiveid= perspective = point = folder = null;

});

awall.controller('CheckoutController', function($stateParams, $scope, $cordovaSQLite) {
	// Get the userid from local storage
	var userid=window.localStorage['userid'];
	
	// will execute when device is ready, or immediately if the device is already ready.
	ionic.Platform.ready(function(){
		// Update the user table with user end_datetime
		var query_update_user = "UPDATE users SET end_datetime=datetime('now') WHERE id= ?";
	    $cordovaSQLite.execute(db, query_update_user, [userid]).then(function(res) {
	    }, function (err) {
	        console.error(err);
	    });
	    
	    // Get the points to use with the postcard
	    var query_get_points = "SELECT point_id, perspective_id FROM visits where user_id=?";
	    $cordovaSQLite.execute(db, query_get_points, [userid]).then(function(res) {
	    	var stamps = [];
	    	if(res.rows.length >= 1){
	    		$scope.perspective = getPerspective(res.rows.item(0).perspective_id);
	    		for (i = 0; i < res.rows.length; ++i) {
	    		    for(j=0; j<points.length; j++){
	    		    	if(points[j]['id']==res.rows.item(i).point_id){
	    		    		stamps.push(points[j]['resource']);
	    		    		break
	    		    	}
	    		    }
	    		    
	    		}
	    		$scope.stamps = stamps;
	    	}
	    }, function (err) {
	        console.error(err);
	    });
	    stamps=null;
	});
	
	$scope.languageid=$stateParams.languageid;
	switch($stateParams.languageid) {
    case "1": // Dutch
    	$scope.title='Uitchecken';
    	$scope.restart = 'Klik hier om opnieuw te starten';
    	 break;
    case "2": // English
    	$scope.title='Checkout';
    	$scope.restart = 'Click here to restart';
    	 break;
    default:
        //do nothing
	} 

});

